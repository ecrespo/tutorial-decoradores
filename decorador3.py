#!/usr/bin/env python


def miDecorador(msg='Mensaje'):

    def decorada(f):

        def wrapper(*args, **kwargs):
            print ('El mensaje es: ' + msg )
            print ("dentro del decorador antes de llamar a la funcion")
            f(*args, **kwargs)
            print("dentro del decorador despues de llamar a la funcion")
        return wrapper

    return decorada

@miDecorador(msg="Te amo ")
def printNombre(nombre):
    print(nombre)

if __name__ == '__main__':
    printNombre("Dayana")
