#!/usr/bin/env python


def miDecorador(f):


    def wrapper(*args, **kwargs):
        print ("dentro del decorador antes de llamar a la funcion")
        f(*args, **kwargs)
        print("dentro del decorador despues de llamar a la funcion")
    return wrapper

@miDecorador
def printNombre(nombre):
    print(nombre)

if __name__ == '__main__':
    printNombre("Dayana")
