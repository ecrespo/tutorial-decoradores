#!/usr/bin/env python


import json
def as_json(func):
    def inner(*args,**kwargs):
        result = func(*args, **kwargs)
        return json.dumps(result)
    return inner

@as_json
def func(x,y):
    return {'resultado': x+y}

if __name__ == '__main__':
    print(func(3,5))
