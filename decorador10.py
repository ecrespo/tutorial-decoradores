#!/usr/bin/env python


def print_call(func):
    def inner(*args,**kwargs):
        print ('llamando {0} con {1} {2}'.format(func.__name__,args,kwargs))
        result = func(*args,**kwargs)
        print('resultado fue {0}'.format(result))
        return result
    return inner

@print_call
def incr(x):
    return x + 1



if __name__ == '__main__':
    val = incr(4)
    print(val)
