#!/usr/bin/env python



def add_by(x):
    def inner(y):
        return x+y
    return inner





if __name__ == '__main__':
    add1 = add_by(1)
    print(add1(4))
    print(add_by(5)(10))
