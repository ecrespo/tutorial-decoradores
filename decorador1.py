#!/usr/bin/env python


def miDecorador(f):


    def wrapper():
        print ("dentro del decorador antes de llamar a la funcion")
        f()
        print("dentro del decorador despues de llamar a la funcion")
    return wrapper

@miDecorador
def printNombre():
    print("Ernesto")

if __name__ == '__main__':
    printNombre()
