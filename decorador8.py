#!/usr/bin/env python



def add_by(x):
    return lambda y: x+y
    




if __name__ == '__main__':
    add1 = add_by(1)
    print(add1(4))
    print(add_by(5)(10))
