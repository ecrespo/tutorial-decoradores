#!/usr/bin/env python




def miDecorador(inner):

    def inner_decorator(*args, **kwargs):
        print (args,kwargs)
    return inner_decorator


@miDecorador
def decorada(dato):
    print ('esto paso ' + dato)

if __name__ == '__main__':
    decorada("Te amo dayana")
