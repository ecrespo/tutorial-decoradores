#!/usr/bin/env python




def miDecorador(inner):

    def inner_decorator(*args, **kwargs):
        print ('Esta funcion toma ' + str(len(args)) + ' argumentos')
        inner(*args)
    return inner_decorator


@miDecorador
def decorada(dato):
    print ('esto paso ' + str(dato))

@miDecorador
def decorada2(num1,num2):
    print ('la suma de ' + str(num1) + ' y ' + str(num2)+ ' es ' + str(num1+num2))

if __name__ == '__main__':
    decorada("es una prueba")
    decorada2(1,2)
